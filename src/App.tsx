import React, { Component } from "react";

import { Switch, Route, Link } from "react-router-dom";
import ProductList from "./Components/ProductList";
import { Layout} from "antd";
import getAllProducts from "../src/api/apis";
import { ProductType } from "./Components/Product";
import NotFound from "./Components/NotFound";
import "./App.css";
import UserMenu from "./Components/UserMenu";
import Cart, { CartType } from "./Components/Cart";
import update from 'immutability-helper';
import Title from "antd/lib/typography/Title";

interface AppProps {}

interface AppState {
  Products: ProductType[];
  Cart: CartType;
}
export default class App extends Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props);
    this.state = {
      Products: [],
      Cart: { CartItems: [] }
    };
  }

  componentDidMount = () => {
    this.getAllProducts();
  };
  getAllProducts = () => {
    getAllProducts().then(response => {
      if (response) {
        let data = response.map((x: any) => {
          return { Name: x.name, Id: x.guid, Price: x.price } as ProductType;
        });
        this.setState({ Products: data });
      }
    });
  };

  updateCart=(product: ProductType)=>{
    const selectedProduct = this.state.Products.find(x => x.Id === product.Id);
    if(selectedProduct!== undefined){
      this.addProductToCart(product);
    }
  }

  addProductToCart = (product: ProductType)=>{
    this.setState({ Cart:{CartItems: [...this.state.Cart!.CartItems, { Product: product, Quantity: 1 }]}});
  }

  updateCartProductQuantity =(newQuantity: number|undefined, productId: string)=>{
    const cartItem = this.state.Cart.CartItems.find(x=>x.Product.Id===productId);
    if(cartItem!==undefined){
      let cartItemsCopy = JSON.parse(JSON.stringify(this.state.Cart.CartItems));
   cartItemsCopy[this.state.Cart.CartItems.indexOf(cartItem)].Quantity=newQuantity;

   this.setState({
      Cart:{CartItems:cartItemsCopy}
    });           
  }
  }

  handleDeleteFromCart=(prodId: string)=>{
    const cartItem = this.state.Cart.CartItems.find(x=>x.Product.Id===prodId);
    if(cartItem!==undefined){
    const index = this.state.Cart.CartItems.indexOf(cartItem);
    let cartItemsCopy = JSON.parse(JSON.stringify(this.state.Cart.CartItems));
    cartItemsCopy.splice(index,1)
    this.setState({
      Cart:{CartItems:cartItemsCopy}
    }); 
    }
  }

  render() {
    const { Header, Content, Footer } = Layout;
    return (
      <Layout className="layout">
        <Header style={{ marginBottom: "20px" }}>
          <Link to="/">
            <img
              src="img/store_logo.png"
              alt=""
              title="Home"
              style={{ width: "80px", height: "30px" }}
            />
          </Link>

          <Link to="/cart">
            <UserMenu />
          </Link>
        </Header>
        <Content style={{ padding: "0 50px" }}>
        {/* <Title>Pro</Title> */}
          <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
            <Switch>
        <Route exact path="/" render={()=><ProductList Cart={this.state.Cart} UpdateCart={this.updateCart}/>} />
              <Route path="/cart" render={()=><Cart Cart={this.state.Cart} updateCart={this.updateCartProductQuantity} deleteFromCart={this.handleDeleteFromCart}/>} />
              <Route component={NotFound} />
            </Switch>
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>A&Z Company ©2019</Footer>
      </Layout>
    );
  }
}
