import axios from "axios";

const baseUrl = "https://localhost:44386/api/";

export function getAllProducts(filter?: string) {
  const searchFilter = filter === undefined ? "" : filter;
  const url = baseUrl + "product" + "?filter=" + searchFilter;
  return axios.get(url).then(response => {
    return response.data;
  });
}

export function createCart(productId: string) {
  const url = baseUrl + "cart";
  const data = {
    productId: productId,
  };
  axios.post(url, data).then(response => {
    return response.data;
  });
}

export default getAllProducts;
