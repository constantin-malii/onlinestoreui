import React, { Component } from "react";
import Product, { ProductType } from "./Product";
import { Input, Button } from "antd";
import * as api from "../../src/api/apis";
import { CartType, CartItem } from "./Cart";
import Title from "antd/lib/typography/Title";


interface ProductListProps { 
  UpdateCart: (product: ProductType)=>void;
  Cart: CartType;
}

interface ProductListState {
  Products: ProductType[];
  SearchCriteria?: string;
  Cart?: CartType;
  CartId?: string;
}

export default class ProductList extends Component<
  ProductListProps,
  ProductListState
  > {
  constructor(props: ProductListProps) {
    super(props);

    this.state = {
      Products: [],
      Cart: { CartItems: [] }
    };
  }
  handleAddToCart = (productId: string) => {
    console.log(productId);
    const selectedProduct = this.state.Products.find( x => x.Id === productId    );
      if (selectedProduct!==undefined&&this.props.Cart && this.props.Cart.CartItems ) {
        if (this.props.Cart.CartItems.length > 0) {
          if(!this.isProductInCart(productId)){
            this.props.UpdateCart(selectedProduct);
          }
        }
          else{
            this.props.UpdateCart(selectedProduct);
          }
      }
  };

  handleSearch = (searchCriteria: string) => {
    this.setState({ SearchCriteria: searchCriteria }, this.getProducts);
  };

  getProducts = () => {
    api.getAllProducts(this.state.SearchCriteria).then(response => {
      if (response) {
        let data = response.map((x: any) => {
          return { Name: x.name, Id: x.guid, Price: x.price } as ProductType;
        });
        this.setState({ Products: data });
      }
    });
  };

  componentDidMount = () => {
    this.getProducts();
  };

  isProductInCart = (productId:string): boolean=>{
    return this.props.Cart!.CartItems.find( (item)=>item.Product.Id===productId)!==undefined;
  }

  render() {
    const Search = Input.Search;
    return (
      <div>
        <Title>Products</Title>
        <Search
          size="large"
          placeholder="Search"
          onSearch={this.handleSearch}
          style={{ marginBottom: "20px" }}
        />
        {this.state.Products && this.state.Products.length > 0 ? (
          this.state.Products.map(x => {
            return (
              <Product
                key={x.Id}
                Name={x.Name}
                Price={x.Price}
                Source="fake source for now"
                 >
                <Button onClick={()=>this.handleAddToCart(x.Id)} disabled={this.isProductInCart(x.Id)}>{this.isProductInCart(x.Id)?"Added to cart":"Add to cart"}</Button> 
              </Product>
            );
          })
        ) : (
            <p>0 products</p>
          )}
      </div>
    );
  }
}
