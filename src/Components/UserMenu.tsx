import React, { Component } from "react";
import {  Icon, Button } from "antd";

interface UserMenuProps {
}

export default class UserMenu extends Component<UserMenuProps, {}> {
  render() {
    return (
      <div className="userMenu">
        <Button size="large" title="Go to cart">
          <Icon type="shopping-cart" />
        </Button>
      </div>
    );
  }
}
