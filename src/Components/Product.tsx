import React from "react";
import { Row, Col, Card,  Statistic } from "antd";

interface ProductProps {
  Name: string;
  Source: string;
  Price: number;
}

export interface ProductType {
  Id: string;
  Name: string;
  Price: number;
}

const Product: React.FC<ProductProps> = ({
  Name,
  Source,
  Price,
  children
}) => {
  return (
    <Card>
      <Row gutter={10}>
        <Col span={6}>
          <img src="img/product.jpg" alt="" style={{ width: "50px" }} />
        </Col>
        <Col span={14}>
          <p style={{ fontSize: "1.5rem" }}>{Name}</p>
          {children}
        </Col>
        <Col span={4}>
          <Statistic title="Price" prefix="$" value={Price} precision={2}/>
        </Col>
      </Row>
    </Card>
  );
};

export default Product;
