import React, { Component } from "react";
import Product, { ProductType } from "./Product";
import { Button, InputNumber, Statistic, Row } from "antd";
import { Link } from "react-router-dom";
import Title from "antd/lib/typography/Title";

interface CartProps {
  Cart: CartType;
  updateCart: (q: number | undefined, prodId: string) => void;
  deleteFromCart: (prodId: string) => void;
}
export type CartType = {
  CartItems: CartItem[];
};

export type CartItem = {
  Product: ProductType;
  Quantity: number;
};
export default class Cart extends Component<CartProps, CartType> {
  render() {
    const isNotEmpty =
      this.props.Cart!.CartItems && this.props.Cart.CartItems.length > 0;
    const productsOrNone = isNotEmpty ? (
      this.props.Cart.CartItems.map(x => {
        return (
          <Product
            key={x.Product.Id}
            Name={x.Product.Name}
            Price={x.Quantity * x.Product.Price}
            Source="fake source for now"
          >
            <span>Quantity:</span>
            <InputNumber
              min={0}
              defaultValue={x.Quantity}
              onChange={num => this.props.updateCart(num, x.Product.Id)}
              style={{marginLeft:"5px"}}
            />
            <Button
              onClick={() => this.props.deleteFromCart(x.Product.Id)}
              icon="delete"
              title="Delete from cart"
              style={{marginLeft:"5px"}}
            />
          </Product>
        );
      })
    ) : (
      <p>
        Empty cart. Add some <Link to="/">Products</Link>
      </p>
    );

    return (
      <>
        <Title>Cart</Title>
        {productsOrNone}
        {isNotEmpty ? (
          <>
            <Row style={{ marginTop: "20px" }}>
              <Statistic
                title="Cart total"
                prefix="$"
                style={{ float: "right" }}
                precision={2}
                value={this.props.Cart.CartItems.reduce(
                  (a, b) => +a + +b.Product.Price * b.Quantity,
                  0
                )}
              />
            </Row>
            <Row style={{ marginTop: "20px" }}>
              <Button size="large" style={{ float: "right" }}>
                Checkout
              </Button>
            </Row>
          </>
        ) : null}
      </>
    );
  }
}
