 1f4ecc4283fc2e12c6ae85ac5028cc5ade9bb051 - last commit before deadline. The rest is just me having fun after.

The solution is broken down into 2 projects:

1. UI project - OnlineStoreUI(React)
2. Api project - OnlineStoreApi(ASP.NET Core 2.2)

Clone the repo from bitbucket:


To start the api project run the solution from OnlineStoreApi folder. Alternatively, dotnet core CLI will work.
.Net core 2.2 is required for the application to run.

To start the UI project ensure the port 3000 is available. The api is configured to allow CORS only from this port/address.
Ensure the latest version of yarn package manager is installed.

cd OnlineStoreUI
yarn install
yarn start

The api solution should build a localdb database and seed with 3 products.
The 3 products are loaded from the db and displayed in the browser.
Products can be added to cart and cart can be displayed.

No authentication and/or authorisation was implemented.